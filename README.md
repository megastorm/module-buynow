# Megastorm Buy Now Module

Add button "Buy Now" for Allow Users Add to Cart and redirect to Checkout page

## Key Features

1. Buy Now for CDP
2. Buy Now for PDP
3. Buy Now for Upsell
4. Buy Now for Related
5. Buy Now for Crosssell
7. Remove other items when click Buy Now
8. Support Widget with new templates (Widgets: Catalog Products List, Catalog New Products List)

## Installation

composer require megastorm/module-buynow

php bin/magento setup:upgrade

php bin/magento setup:static-content:deploy

Go to Store > Configuration > Megastorm > Buy Now

---