<?php
/**
 * Megastorm_Buynow
 *
 * @category    Megastorm
 * @package     Megastorm_Buynow
 * @author      Megastorm Team <megastormweb@gmail.com>
 * @copyright   Megastorm (https://www.megastormweb.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Megastorm\Buynow\Block\Catalog\Product\ProductList\Item;

use Magento\Catalog\Block\Product\Context;
use Megastorm\Base\Helper\Data as MstHelperBase;
use Magento\Framework\Url\Helper\Data as UrlHelper;

class Buynow extends \Magento\Catalog\Block\Product\ProductList\Item\Block
{
    /*
     * @var MstHelperBase
     */
    protected $_mstHelperBase;

    /*
     * @var UrlHelper
     */
    protected $urlHelper;

    /**
     * @param Context $context
     * @param MstHelperBase $mstHelperBase
     * @param UrlHelper $urlHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        MstHelperBase $mstHelperBase,
        UrlHelper $urlHelper,
        array $data = [])
    {
        $this->_mstHelperBase = $mstHelperBase;
        $this->urlHelper = $urlHelper;
        parent::__construct($context, $data);
    }

    /**
     * Get post parameters
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getAddToCartPostParams(\Magento\Catalog\Model\Product $product)
    {
        $url = $this->getAddToCartUrl($product);
        return [
            'action' => $url,
            'data' => [
                'product' => $product->getEntityId(),
                \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED => $this->urlHelper->getEncodedUrl($url),
            ]
        ];
    }
}
